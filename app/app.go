package app

import (
	"airports/models"
	"airports/utils"
	"airports/utils/calculator"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

type App struct {
	center  *models.Fields
	radius  float64
	baseUrl string
}

func NewApp() App {
	lon, lat, rad := utils.ParseFlags()

	app := App{
		baseUrl: "https://mikerhodes.cloudant.com/airportdb/_design/view1/_search/geo",
		center: &models.Fields{
			Lat: lat,
			Lon: lon,
		},
		radius: rad,
	}

	return app
}

const MAX_LIMIT = 200

func (a App) Run() error {
	allAirports := make([]*models.Airport, 0)
	var airportResponse *models.AirportResponse
	var err error

	airportResponse, err = a.createAndSendRequest("", MAX_LIMIT)
	if err != nil {
		return err
	}

	allAirports = append(allAirports, airportResponse.Rows...)
	rows := airportResponse.TotalRows - len(airportResponse.Rows)
	for rows > 0 {
		limit := MAX_LIMIT
		if rows < MAX_LIMIT {
			limit = rows
		}
		airportResponse, err = a.createAndSendRequest(airportResponse.Bookmark, limit)
		if err != nil {
			return err
		}
		allAirports = append(allAirports, airportResponse.Rows...)
		rows -= len(airportResponse.Rows)
	}
	radAirports := calculator.FilterToRadius(a.center, a.radius, allAirports)
	calculator.CalculateDistances(a.center, radAirports)
	utils.SortAirports(radAirports)

	log.Printf("center: lon %2.2f, lat %2.2f; Radius: %2.2f\n", a.center.Lon, a.center.Lat, a.radius)
	log.Println("Idx, Name, Distance")
	for i, airport := range radAirports {
		log.Printf("%d: %v, %2.2fkm\n", i, airport.Fields.Name, airport.Distance)
	}

	return nil
}

func (a App) getUrlWithQuery(bookmark string, limit int) string {
	lonLow := a.center.Lon - a.radius
	lonHigh := a.center.Lon + a.radius
	latLow := a.center.Lat - a.radius
	latHigh := a.center.Lat + a.radius

	query := fmt.Sprintf("lon:[%f TO %f] AND lat:[%f TO %f]&limit=%d", lonLow, lonHigh, latLow, latHigh, limit)
	if bookmark != "" {
		query += fmt.Sprintf("&bookmark=%s", bookmark)
	}

	return fmt.Sprintf("%s?q=%s", a.baseUrl, url.PathEscape(query))
}

func (a App) getResponse(uri string) (*http.Response, error){
	return http.Get(uri)
}

func (a App) createAndSendRequest(bookmark string, limit int) (*models.AirportResponse, error){
	urlWithQuery := a.getUrlWithQuery(bookmark, limit)
	resp, err := a.getResponse(urlWithQuery)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()


	airportResponse, err := utils.ParseResponse(resp)
	if err != nil {
		return nil, err
	}
	if len(airportResponse.Rows) == 0 {
		return nil, fmt.Errorf("Response did not cointain any airports.")
	}

	return airportResponse, nil
}
FROM golang:1.11

WORKDIR /go/src
COPY . ./airports

WORKDIR /go/src/airports

RUN go get -v github.com/stretchr/testify

ENTRYPOINT ["go", "test",  "-v", "./..."]

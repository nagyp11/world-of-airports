package calculator

import (
	"airports/models"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestData struct {
	Airport *models.Airport
	Expected interface{}
}

func TestCalculateDistances(t *testing.T) {
	testCenter := &models.Fields{Lat: 0, Lon: 0}
	testData := []TestData{
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: 0, Lon: 0}},
			Expected: 0,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: -90, Lon: -180}},
			Expected: 10010,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: +90, Lon: +180}},
			Expected: 10010,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: 15, Lon: 27}},
			Expected: 3404,
		},
	}
	testAirports := make([]*models.Airport, 0)
	for _, td := range testData {
		testAirports = append(testAirports, td.Airport)
	}

	CalculateDistances(testCenter, testAirports)

	for i, calculated := range testAirports {
		assert.InDelta(t, calculated.Distance, testData[i].Expected, 3)
	}
}

func TestFilterToRadius(t *testing.T) {
	testCenter := &models.Fields{Lat: 0, Lon: 0}
	var testRadius float64 = 32
	testData := []TestData{
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: 0, Lon: 0}},
			Expected: true,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: -90, Lon: -180}},
			Expected: false,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: +90, Lon: +180}},
			Expected: false,
		},
		{
			Airport: &models.Airport{Fields: &models.Fields{Lat: 15, Lon: 27}},
			Expected: true,
		},
	}
	testAirports := make([]*models.Airport, 0)
	expected := make([]*models.Airport, 0)
	for _, td := range testData {
		testAirports = append(testAirports, td.Airport)
		if td.Expected == true {
			expected = append(expected, td.Airport)
		}
	}

	filteredAirports := FilterToRadius(testCenter, testRadius, testAirports)


	assert.Equal(t, expected, filteredAirports)
}
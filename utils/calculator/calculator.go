package calculator

import (
	"airports/models"
	"math"
)


func CalculateDistances(center *models.Fields, airports []*models.Airport) {
	for _, airport := range airports {
		airport.Distance = haversine(airport.Fields, center)
	}
}

func FilterToRadius(center *models.Fields, rad float64, airports []*models.Airport) []*models.Airport {
	filtered := make([]*models.Airport, 0)

	for _, airport := range airports {
		if dist(airport.Fields, center) <= rad {
			filtered = append(filtered, airport)
		}
	}

	return filtered
}

func dist(latlon1, latlon2 *models.Fields) float64 {
	return math.Sqrt(math.Pow(latlon2.Lat - latlon1.Lat, 2) + math.Pow(latlon2.Lon - latlon1.Lon, 2))
}

/*
The Pythagoras’ theorem could be sufficient if the accuracy is not that important, but I found this formula to
calculate a quite accurate distance.
The haversine formula determines the great-circle distance between two points on a sphere given their
longitudes and latitudes
R is the earth's radius
More info: https://en.wikipedia.org/wiki/Haversine_formula
*/
const R = 6371

func haversine(latlon1, latlon2 *models.Fields) float64 {
	var phi1 = toRadian(latlon1.Lat)
	var phi2 = toRadian(latlon2.Lat)
	var deltaPhi = toRadian(latlon2.Lat - latlon1.Lat)
	var deltaLambda = toRadian(latlon2.Lon - latlon1.Lon)

	var a = math.Sin(deltaPhi/2) * math.Sin(deltaPhi/2) +
		math.Cos(phi1) * math.Cos(phi2) *
		math.Sin(deltaLambda/2) * math.Sin(deltaLambda/2)
	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return R * c
}

func toRadian(angle float64) float64 {
	return angle * math.Pi / 180
}
package utils

import (
	"airports/models"
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestParseFlags(t *testing.T) {
	lon,lat,rad := ParseFlags()

	assert.Equal(t,float64(10.12),lon, "should've return default value")
	assert.Equal(t,float64(15),lat, "should've return default value")
	assert.Equal(t,float64(3), rad, "should've return default value"	)
}

func TestParseResponse(t *testing.T) {
	expectedAirportResponse := &models.AirportResponse{
		TotalRows: 1,
		Bookmark: "my-bookmark",
		Rows: []*models.Airport{
			{
				ID: "aa915feea5ecf2f87d0c7bca672da2d5",
				Order: []float64{
					1.4142135381698608,
					0,
				},
				Fields: &models.Fields{
					Lat: 53.630389,
					Lon: 9.988228,
					Name: "Hamburg",
				},
			},
		},
	}
	readCloser := ioutil.NopCloser(bytes.NewReader([]byte("{\"total_rows\":1,\"bookmark\":\"my-bookmark\",\"rows\":[{\"id\":\"aa915feea5ecf2f87d0c7bca672da2d5\",\"order\":[1.4142135381698608,0],\"fields\":{\"lat\":53.630389,\"lon\":9.988228,\"name\":\"Hamburg\"}}]}")))
	testResponse := &http.Response{
		Body: readCloser,
	}

	airportResponse, err := ParseResponse(testResponse)
	assert.Nil(t, err, "should not have error")
	assert.Equal(t, expectedAirportResponse, airportResponse)
}

func TestParseResponseFail(t *testing.T) {
	readCloser := ioutil.NopCloser(bytes.NewReader([]byte("")))
	testResponse := &http.Response{
		Body: readCloser,
	}

	_, err := ParseResponse(testResponse)
	assert.NotNil(t, err, "should not have error")
}

func TestSortAirports(t *testing.T) {
	testAirports := []*models.Airport{
		{Distance: 25},
		{Distance: 15},
		{Distance: 10},
		{Distance: 10},
  }

  SortAirports(testAirports)

	assert.Equal(t, testAirports[0].Distance, float64(10))
	assert.Equal(t, testAirports[1].Distance, float64(10))
	assert.Equal(t, testAirports[2].Distance, float64(15))
	assert.Equal(t, testAirports[3].Distance, float64(25))
}
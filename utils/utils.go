package utils

import (
	"airports/models"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"sort"
)

func ParseResponse(resp *http.Response) (*models.AirportResponse, error) {
	target := &models.AirportResponse{}

	if err := json.NewDecoder(resp.Body).Decode(target);err != nil {
		return nil, err
	}

	return target, nil
}

func ParseFlags() (lon, lat, rad float64) {
	lonFlag := flag.Float64("lon", 10.12, "Longitude (-180 to 180)")
	latFlag := flag.Float64("lat", 15, "Latitude (-90 to 90)")
	radFlag := flag.Float64("rad", 3, "Radius")

	flag.Parse()

	if *lonFlag > 180 || *lonFlag < -180 {
		log.Print("Given longitude is out of valid range, using default")
		*lonFlag = 10.12
	}
	if *latFlag > 90 || *latFlag < -90 {
		log.Print("Given latitude is out of valid range, using default")
		*latFlag = 15
	}


	lon, lat, rad = *lonFlag, *latFlag, *radFlag

	return
}

func SortAirports(airports []*models.Airport) {
	sort.Slice(airports[:], func(i, j int) bool {
		return airports[i].Distance < airports[j].Distance
	})
}
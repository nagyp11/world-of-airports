package models

type Fields struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
	Name string `json:"name"`
}

type Airport struct {
	ID string `json:"id"`
	Order []float64 `json:"order"`
	Fields *Fields `json:"fields"`
	Distance float64
}

type AirportResponse struct {
	TotalRows int `json:"total_rows"`
	Bookmark string `json:"bookmark"`
	Rows []*Airport `json:"rows"`
}
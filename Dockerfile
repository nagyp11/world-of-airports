FROM golang:1.11

WORKDIR /go/src
COPY . ./airports

WORKDIR /go/src/airports

RUN go build -o main main.go

ENTRYPOINT ["./main"]

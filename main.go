package main

import (
	"airports/app"
	"log"
)

func main() {
	myApp := app.NewApp()

	if err := myApp.Run(); err != nil {
		log.Fatal("Error: ", err)
	}
}


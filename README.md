# World of Airports
An application that pulls data about airports from a Cloudant database and displays their location in a simple list, sorted by distance within a user provided radius of a user provided lat/long point.
## Getting Started
### Building and running
#### In container
```
docker build -t <tag> .
docker run --rm <tag> -lon 40 -lat 10 -rad 5
```
#### Locally
```
go build -o main main.go
./main -lat 15 -lon 15 -rad 5
```
#### For help use
```
./main -h
```
All the flags are optional, if any of it is not present the default value will be used
## Running the tests
### In container
```
docker build -t <tag> -f test.Dockerfile .
docker run <tag>
```
### Locally
```
go get -v github.com/stretchr/testify
go test -v ./...
```
## Author(s)
* **Péter Nagy**
